var config = {
    paths: {
        'imagesloaded': 'Smartwave_Porto/js/imagesloaded',
        'packery': 'Smartwave_Porto/js/packery.pkgd'
    },
    shim: {
        'imagesloaded': {
            deps: ['jquery']
        },
        'packery': {
            deps: ['jquery']
        }
    },
    map: {
        '*': {
            checkoutjs: 'Magento_Checkout/js/autocomplete.js'
        }
    }
};
