<?php
/**
* Copyright © 2018 Default. All rights reserved.
*/

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'frontend/Troquer/default',
    __DIR__
);
