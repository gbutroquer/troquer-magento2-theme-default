define([
    'jquery',
    'Magento_Checkout/js/checkout-data',
    'uiRegistry',
    'uiComponent'
], function(
    $,
    checkoutData,
    uiRegistry,
    Component
) {
    $(document).ready(function() {
        let existCondition = setInterval(function() {
            if ($("input[name='postcode']").length) {
                clearInterval(existCondition);
                runMyFunction();
            }
        }, 500);

        function runMyFunction() {
            let postcode = $("input[name='postcode']");
            let valcp = postcode.val();
            if (valcp !== '') {
                selectAddress(valcp);
            }
            postcode.on("keyup", function() {
                let lengthcp = $(this).val().length;
                let code_cp = $(this).val();
                if (lengthcp === 5) {
                    selectAddress(code_cp);
                }
            });

            $("[name='shippingAddress.street.0'] > .label").text('Calle');
            $("[name='shippingAddress.street.1'] > .label").text('Número Exterior');
            $("[name='shippingAddress.street.2'] > .label").text('Número Interior');
            $("div[name='shippingAddress.custom_attributes.suburb'] > .control > input.input-text").after('<select id="suburb_id" name="suburb_id" class="form-control"></select>');
            $("div[name='shippingAddress.postcode'] > .label").before('<p class="legend_cp">Escribe los 5 dígitos de tu código postal para mostrar tu colonia, ej: <strong>52178, 03810.</strong></p>');

            $("input[name='city']").addClass('field_address_bloq');
            $(".field._required.streetFirstID").addClass('show_inpt');
            $(".field.additional.streetSecondID").addClass('show_inpt');
            $(".field.additional.streetThirdID").addClass('show_inpt');
            $("select[name='region_id']").addClass('field_address_bloq');
            let is_for_sales = $("[name='custom_attributes[is_for_sales]']")
            if(is_for_sales.is(':checked')) {
                is_for_sales.click();
            }

            function selectAddress(cpcode) {
                $.ajax({
                    url: "/customer/address/addresscp",
                    type: "POST",
                    showLoader: true,
                    cache: true,
                    data: { zip_code: cpcode },

                    success: function(response) {
                        $("#suburb_id").empty().append("<option value=''>Selecciona tu colonia</option>");
                        Object.keys(response[0]["neighborhood"]).forEach(function(key) {
                            let option = response[0]["neighborhood"][key];
                            $("#suburb_id").append("<option value='" + option["colonia"] + "'>" + option["colonia"] + "</option>");
                        });
                        if(response[0].region) {
                            $("select[name='region_id'] option:contains('" + response[0].region["region_name"] + "')").each(function () {
                                if($(this).text() === response[0].region["region_name"]){
                                    $(this).attr('selected', true).change();
                                }
                            });
                            $("select[name='region_id']").change();
                            $("[name='region']").val(response[0].region["region_name"]).change();
                        }
                        let suburb = $("[name='custom_attributes[suburb]']");
                        let suburbedit = suburb.val();
                        if(suburbedit) {
                            suburb.change();
                        } else if(response[0]["neighborhood"].length) {
                            let suburbedit = response[0]["neighborhood"][0]["colonia"];
                            suburb.val(suburbedit).change();
                        }
                        $("#suburb_id option:contains('" + suburbedit + "')").attr('selected', true);
                        if(response[0]["municipality"]) {
                            let city = $("input[name='city']");
                            city.val(response[0]["municipality"]["municipality_name"]);
                            city.change();
                        }
                    },
                    error: function(response) {}
                });
            }

            $('#suburb_id').on('change', function() {
                let suburb = $("[name='custom_attributes[suburb]']");
                suburb.val(this.value);
                suburb.change();
            });
        }
    });

    return Component;
});
